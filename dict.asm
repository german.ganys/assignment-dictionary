section .text
extern string_equals

global find_word

%define STR_EQ_SUCCESS 1

; find_word принимает два аргумента:
;   rdi - Указатель на нуль-терминированную строку
;   rsi - Указатель на начало словаря
; Производит поиск по связному списку и выводит value если находит key
find_word:
    push rsi
    push rdi
    ; Добавляем 8, тк узел листа имеет такой вид
    ; |----------------|---------------------|
    ; | next (8 bytes) | string \0 (n bytes) |
    ; |----------------|---------------------|
    add rsi, 8

    call string_equals

    pop rdi
    pop rsi
    cmp rax, STR_EQ_SUCCESS
        je .success

    ; head = head->next
    mov rsi, [rsi]

    cmp rsi, 0
        je .fail

    jmp find_word

    .success:
	    mov rax, rsi
        ret

    .fail:
	    mov rax, 0
        ret