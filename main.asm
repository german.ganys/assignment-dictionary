%include "colon.inc"

extern exit

extern string_length
extern string_equals

extern string_copy

extern read_word

extern find_word

extern print_string
extern print_string_err

%define NEW_LINE 0xA
%define INPUT_LIMIT 255
%define READ_ERROR 0
%define FIND_WORD_ERROR 0

section .data
    %include "words.inc"
    buffer: times INPUT_LIMIT db 0
    welcome_str: db "Provide key: ", 0
    dict_miss_str: db "Key error", NEW_LINE, 0
    input_ov_str: db "Key lengh overflow (must be from 0 to 255)", NEW_LINE, 0

global _start

section .text

_start:
    mov rdi, welcome_str
    call print_string

    mov rdi, buffer
    mov rsi, INPUT_LIMIT

    call read_word

    cmp rax, READ_ERROR
        je .print_input_ov_str


    mov rdi, rax
    mov rsi, ptr
    call find_word

    cmp rax, FIND_WORD_ERROR
        je .print_dict_miss_str

    ; Добавляем 8, тк узел листа имеет такой вид
    ; |----------------|---------------------|---
    ; | next (8 bytes) | key \0 (n bytes)    | value \0...
    ; |----------------|---------------------|---
    add rax, 8

    mov rdi, rax

    push rdi
    call string_length ; находим длину ключа
    pop rdi

    add rdi, rax ; пропускаем, чтобы вывести значение
    inc rdi

    call print_string
    call exit

    .print_dict_miss_str:
        mov rdi, dict_miss_str
        call print_string_err
        call exit

    .print_input_ov_str:
        mov rdi, input_ov_str
        call print_string_err
        call exit

