AS=nasm
ASFLAGS=-f elf64 -o

binary: main.o lib.o dict.o
	ld main.o lib.o dict.o -o binary

%.o: %.asm
	$(AS) $(ASFLAGS) $@ $<

clean:
	rm -rf *.o binary

dev_start: clean binary
	exec ./binary

.PHONY: binary clean
